# Snake JS

This is my first game. Created for training purposes.

	control
	- WASD
	- arrows - ↑← ↓→
	- P - Pause
	update 1.0:
		- added big food;
		- added random;
		- fixed stability;